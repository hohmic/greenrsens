# ![Logo GreenrSens](res/logoMin.png ) GreenrSens
This is a smart open-source environmental sensing device for Smart Gardening solutions.
A *Bluetooth Low Energy* SoC Unit can advertise the sensor data to any other Bluetooth LE compatible device, like Smart-phones or a Raspberry Pi 4. With a CR2032 battery cell, the GreenrSens can run on energy for up to a year.

![Rendered Sample Video](res/pcb.mp4)

## Characteristics

<details>
  <summary>A short summary of the characteristics of the sensor</summary>

### Sensors
Included are 4 different environmental sensors:

- soil-moisture (based on a capacitive measurement with a **oscillating 74HC14D** Schmitt-Trigger)
- ground temperature **MCP9808**
- ambient light sensor **BH1750**
- air temperature and humidity **SI7006**

### Control Unit

The main control unit is a [**BlueNRG-1**](https://www.st.com/en/wireless-transceivers-mcus-and-modules/bluenrg-1.html) from STMicroelectronics.. This is a very low power Bluetooth low energy (BLE) single-mode system-on-chip including a *Cortex M0* to run the user application code.

![ST BlueNRG-1](res/BlueNRG1.jpg)

### Battery

The whole sensor is driven by a single **CR2032** button cell. Due to a average Bluetooth advertisement current consumption of lower than 20uA, a battery time of over a year can be achieved (advertisement interval 1000ms).

### Waterproof

A surrounding resin cover protects the circuitry from water and humidity.  

</details>

## PCB

<details>
  <summary> A description of the pcb</summary>
The [pcb](pcb) folder contains Autodesk EAGLE 9.6 design files, including the 3D models of the ICs.

![PCB front view](res/pcb_front.png)

The *gerberfile* can be found in the gerber folder. This folder contains also the BOM file and a parts-placing-sheet for the manufacturer *JLC-PCB*. 

</details>



## Source Code

<details>
  <summary>The corresponding code</summary>


### BlueNRG-1 Firmware

upload coming soon ...

### Sample Raspberry Pi 4 Python Code

As a base station a Raspberry Pi 4 can be used to collect data from the Sensor-Devices and share it in the cloud.

upload coming soon ...

</details>


## Credits

For questions or more information visit my website [hohenegger.tech](https://www.hohenegger.tech)